# -*- coding: utf-8 -*-
from functools import wraps

import zmq
from flask import Flask, send_from_directory, request
from flask_login import LoginManager, current_user
from flask_sqlalchemy import SQLAlchemy
from flask_toastr import Toastr
from flask_wtf.csrf import CSRFProtect
from werkzeug.contrib.fixers import ProxyFix
from flask_migrate import Migrate
from flask_cors import CORS
import threading
import time
from filter import *
import sys, os
from flask_jwt_extended import JWTManager, jwt_required, create_access_token,get_jwt_identity

app = Flask(__name__, static_folder='static', static_url_path='')

app.wsgi_app = ProxyFix(app.wsgi_app)
app.secret_key = "super secret key"
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy   dog'
app.config['CORS_HEADERS'] = 'Content-Type'

project_root = os.path.dirname(os.path.dirname(__file__))


basedir = os.path.abspath(os.path.dirname(__file__))
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')

UPLOAD_FOLDER = basedir+"\\static\\img"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


db_host = 'localhost'
db_user = 'root'
db_pass = 'vuvanbac1997'
# db_pass = ''
db_name = 'api_flask_template'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + db_user + ':' + db_pass + '@' + db_host + '/' + db_name

login_manager = LoginManager(app)
login_manager.session_protection = "strong"
db = SQLAlchemy()
csrf = CSRFProtect()
toastr = Toastr()
migrate = Migrate()
cors = CORS()
app.config['JWT_SECRET_KEY'] = '\x0b\x04X\t\xd4xH\xb5\xb6\x84\x82\xbe\t\xe0\xebdCI\x93\x01`s`\xda'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)


blacklist = set()
@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return jti in blacklist

list_ip = []

from model.user import User
from model.record_patient import Record
from model.appointment import Appointment
from model.specialist import Specialist
from model.image import Image
from model.clinic import Clinic
from model.prescription import Prescription
from model.notification import Notification


@app.route('/robots.txt')
@app.route('/sitemap.xml')
def static_from_root():
    return send_from_directory(app.static_folder, request.path[1:])


def client_check():
    print("checking...")
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:5555")
    message = socket.recv()
    print("server receiver")
    print("Received reply: %s" % (message.decode()))
    socket.send_string("server receiver")
    list_ip.append(message.decode())


label = {}


# label['Thở sâu'] = 1
# label['Bình thường'] = 1
# label['Thở mạnh'] = 1


def receiver_label():
    print("label checking...")
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:9999")
    while True:
        message = socket.recv()
        label_receiver = message.decode()
        label[label_receiver] += 1
        if label_receiver == "Thở sâu" and label[label_receiver] == 10:
            label[label_receiver] = 0
        if label_receiver == "Thở mạnh" and label[label_receiver] == 10:
            print(label[label_receiver])
            label[label_receiver] = 0
        # print("Received reply: %s" % (message.decode()))
        socket.send_string("server receiver")


@login_manager.user_loader
def load_user(user_id):
    # return User.get(user_id)
    return User.query.get(int(user_id))


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated():
                return login_manager.unauthorized()
            if ((current_user.role not in role) and (role != "ANY")):
                return login_manager.unauthorized()
            return fn(*args, **kwargs)

        return decorated_view

    return wrapper


def filer_register():
    app.jinja_env.filters['datetime'] = datetimeformatStr
    app.jinja_env.filters['timestamp'] = datetimeformatTime


def create_app():
    # login_manager.init_app(app)
    toastr.init_app(app)
    csrf.init_app(app)
    db.init_app(app)
    cors.init_app(app)
    with app.app_context():
        db.create_all()

    migrate.init_app(app, db)

    # Admin
    from route.auth.auth import auth
    from route.admin.admin import user
    from route.admin.ajax import ajax_admin
    from route.admin.admin_doctor import admin_doctor
    from route.admin.admin_chat import admin_chat
    from route.admin.admin_clinic import admin_clinic
    from route.admin.admin_prescription import admin_prescription
    from route.admin.admin_specialist import admin_specialist

    # Patient
    from route.patient.patient import patient
    from route.patient.patient_appointment import patient_appointment
    from route.patient.ajax import ajax_patient
    from route.patient.patient_notification import patient_notification

    # Common
    from route.common.profile import profile
    from route.common.upload import upload

    # Api
    from route.api.api import api
    csrf.exempt(api)


    app.register_blueprint(auth)
    app.register_blueprint(admin_prescription)
    app.register_blueprint(admin_specialist)
    app.register_blueprint(user)
    app.register_blueprint(patient)
    app.register_blueprint(patient_appointment)
    app.register_blueprint(ajax_patient)
    app.register_blueprint(ajax_admin)
    app.register_blueprint(admin_doctor)
    app.register_blueprint(admin_chat)
    app.register_blueprint(admin_clinic)
    app.register_blueprint(api)
    app.register_blueprint(profile)
    app.register_blueprint(upload)
    app.register_blueprint(patient_notification)

    filer_register()
    # thread = threading.Thread(target=receiver_label)
    # thread1 = threading.Thread(target=client_check)
    # thread.start()
    # thread1.start()

    return app
