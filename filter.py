from dateutil.parser import parse
from datetime import datetime

def datetimeformatStr(value, format='%Y-%m-%dT%H:%M:%S'):
    time = parse(value)
    format = '%H:%M %d/%m/%Y'
    return datetime.strftime(time, format)

def datetimeformatTime(value, format=' %H:%M %d/%m/%Y'):
    date = datetime.strftime(value, format)
    return date
