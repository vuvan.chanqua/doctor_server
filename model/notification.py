from init import db
from datetime import datetime


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    recipient_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    object_id = db.Column(db.Integer, db.ForeignKey('appointment.id'))
    object_type =db.Column(db.Integer)
    title =db.Column(db.String(120))
    body = db.Column(db.String(250))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    is_read = db.Column(db.Integer, default=0)

    def __repr__(self):
        return '<Notification {}>'.format(self.body)
