from init import db
from datetime import datetime


class Clinic(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    address = db.Column(db.String(120))
    description = db.Column(db.Text())
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    doctor = db.relationship("User",  foreign_keys='User.clinic_id', backref="doctor", lazy='dynamic')
    # image_id = db.Column(db.Integer, db.ForeignKey('image.id'))

