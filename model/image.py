from init import db
from datetime import datetime


class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(120))
    name = db.Column(db.String(120))
    path = db.Column(db.String(120))
    uploaded = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    owner = db.Column(db.String(120))

    user_image = db.relationship('User',
                                 foreign_keys='User.image_id',
                                 backref='image', lazy='dynamic')
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    # clinic_image = db.relationship('Clinic',
    #                                foreign_keys='Clinic.image_id',
    #                                backref='image', lazy='dynamic')

    def __repr__(self):
        return '<Image {}>'.format(self.name)
