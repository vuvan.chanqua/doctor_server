from datetime import datetime

from init import db
from model.message import Message


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120))
    name = db.Column(db.String(120))
    password = db.Column(db.String(120))
    ip = db.Column(db.String(120), nullable=True)
    role = db.Column(db.String(120))
    address = db.Column(db.String(120), nullable=True)
    id_card = db.Column(db.String(120), nullable=True)
    phone_number = db.Column(db.String(120), nullable=True)
    gender = db.Column(db.Integer, nullable=True)
    date_of_birth = db.Column(db.String(120), nullable=True)
    age = db.Column(db.Integer, nullable=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    image_id = db.Column(db.Integer, db.ForeignKey('image.id'), default=12)

    records = db.relationship('Record', backref='user', lazy=True)

    clinic_id = db.Column(db.Integer, db.ForeignKey('clinic.id'))
    specialist_id = db.Column(db.Integer, db.ForeignKey('specialist.id'))

    messages_sent = db.relationship('Message',
                                    foreign_keys='Message.sender_id',
                                    backref='author', lazy='dynamic')

    messages_received = db.relationship('Message',
                                        foreign_keys='Message.recipient_id',
                                        backref='recipient', lazy='dynamic')

    notification_sent = db.relationship('Notification',
                                    foreign_keys='Notification.sender_id',
                                    backref='sender', lazy='dynamic')

    notification_received = db.relationship('Notification',
                                        foreign_keys='Notification.recipient_id',
                                        backref='recipient', lazy='dynamic')

    appointment_patient = db.relationship('Appointment',
                                          foreign_keys='Appointment.patient_id',
                                          backref='patient', lazy='dynamic')

    appointment_admin = db.relationship('Appointment',
                                        foreign_keys='Appointment.doctor_id',
                                        backref='doctor', lazy='dynamic')

    appointment_prescription_admin = db.relationship('Prescription',
                                                     foreign_keys='Prescription.doctor_id',
                                                     backref='doctor', lazy='dynamic')

    appointment_prescription_patient = db.relationship('Prescription',
                                                       foreign_keys='Prescription.patient_id',
                                                       backref='patient', lazy='dynamic')
    # last_message_read_time = db.Column(db.DateTime)

    # def new_messages(self):
    #     last_read_time = self.last_message_read_time or datetime(1900, 1, 1)
    #     return Message.query.filter_by(recipient=self).filter(
    #         Message.timestamp > last_read_time).count()

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def get_urole(self):
        return self.role

    def __repr__(self):
        return '<User %r>' % self.id
