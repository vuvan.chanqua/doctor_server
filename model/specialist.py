from init import db
from datetime import datetime


class Specialist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    description = db.Column(db.Text())
    doctor_specialist = db.relationship("User", foreign_keys='User.specialist_id', backref="doctor_specialist", lazy='dynamic')
    # appointment_specialist = db.relationship("Appointment", foreign_keys='Appointment.specialist_id', backref="appointment_specialist", lazy='dynamic')

    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

