from init import db
from datetime import datetime

class Appointment(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    patient_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    doctor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    # specialist_id = db.Column(db.Integer, db.ForeignKey('specialist.id'))
    title = db.Column(db.String(255))
    content = db.Column(db.Text)
    time_start = db.Column(db.String(120))
    time_finish = db.Column(db.String(120))

    all_day = db.Column(db.String(120))

    status = db.Column(db.Integer, default=1)
    #     active 1
    #     deactive 0

    appointment_prescription = db.relationship('Prescription',
                                               foreign_keys='Prescription.appointment_id',
                                               backref='prescription', lazy='dynamic')

    appointment_notification = db.relationship('Notification',
                                               foreign_keys='Notification.object_id',
                                               backref='appointment', lazy='dynamic')
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
