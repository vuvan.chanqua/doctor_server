import collections
import socket
from threading import Thread
from time import sleep

import sounddevice as sd
import zmq

from config import SAMPLING_RATE, BLOCK_SIZE, PREDICTION_STEP

host_name = socket.gethostname()
host_ip = socket.gethostbyname(host_name)
local_host_ip = socket.gethostbyname("localhost")
#

# from tornado import gen
# from tornado.ioloop import IOLoop
# from tornado.tcpclient import TCPClient
#
# FORMAT = pyaudio.paInt16
# CHANNELS = 2
# RATE = 44100
# CHUNK = 1024
# RECORD_SECONDS = 15
# FILE_NAME = "RECORDING.wav"
# frames = []
# audio = pyaudio.PyAudio()  # instantiate the pyaudio
# stream_audio = audio.open(format=FORMAT, channels=CHANNELS,
#                           rate=RATE,
#                           input=True,
#                           frames_per_buffer=CHUNK)

audio_queue = collections.deque(maxlen=1000)  # Queue for incoming audio blocks
live_audio_feed = collections.deque(maxlen=1)


def capture_audio(block, block_len, time, status):
    audio_queue.append(block.copy())


def send_array(socket, A, flags=0, copy=True, track=False):
    """send a numpy array with metadata"""
    md = dict(
        dtype=str(A.dtype),
        shape=A.shape,
    )
    socket.send_json(md, flags | zmq.SNDMORE)
    return socket.send(A, flags, copy=copy, track=track)


def return_check_by_server():
    while True:
        context = zmq.Context()
        skt = context.socket(zmq.REQ)
        skt.connect("tcp://127.0.0.1:5555")
        print(local_host_ip)
        skt.send_string(local_host_ip)
        message = skt.recv()
        print("Received reply: %s" % (message.decode()))


# def send_ip_to_register():
#     context = zmq.Context()
#     print("Connecting to hello world server")
#     skt = context.socket(zmq.REQ)
#     skt.connect("tcp://localhost:5555")
#     skt.send_string(host_ip)
#     message = skt.recv()



def send_sound_data():
    ctx = zmq.Context()
    skt = ctx.socket(zmq.PUB)
    skt.bind('tcp://127.0.0.1:9000')
    print("start sending")
    # message = skt.recv()
    # print(message)
    # waveform_gen = waveform_gen()

    stream = sd.InputStream(channels=1, dtype='float32', callback=capture_audio,
                            samplerate=SAMPLING_RATE, blocksize=BLOCK_SIZE)
    stream.start()

    blocks = []
    processing_queue = collections.deque()
    while True:
        # data1 = stream_audio.read(CHUNK)
        # data_chunk = array('h', data1)
        # vol = max(data_chunk)
        # sending data rate ~2 Hz

        while len(audio_queue) > 0 and len(blocks) < PREDICTION_STEP:
            send_array(skt, audio_queue.popleft())
        #     blocks.append(audio_queue.popleft())
        #
        # if len(blocks) == PREDICTION_STEP:
        #     new_audio = np.concatenate(blocks)
        #     print(new_audio)
        #     send_array(skt, new_audio)
        # # Populate audio for live streaming
        # live_audio_feed.append(new_audio[:, 0].copy())
        #
        # blocks = []
        # processing_queue.append(new_audio)

        # skt.send_string(str(new_audio))

        sleep(0.05)


if __name__ == "__main__":
    # send_sound_data()
    # send_ip_to_register()
    # t_ip = Thread(target=send_ip_to_register)
    t_sound = Thread(target=send_sound_data)
    t_return = Thread(target=return_check_by_server)
    t_return.start()
    t_sound.start()
    # t_ip.start()

