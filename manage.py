from flask_migrate import MigrateCommand, Manager
from init import create_app, db
from model.user import User
from werkzeug.security import generate_password_hash
from faker import Faker
fake = Faker()

app = create_app()
manager = Manager(app)

manager.add_option("-c", "--config", dest="config_module", required=False)
manager.add_command('db', MigrateCommand)


@manager.command
def seed():
    # user = User(email="admin", password=generate_password_hash("admin", method='sha256'), role="0", name="admin")
    # user1 = User(email="doctor", password=generate_password_hash("doctor", method='sha256'), role="1", name="doctor")
    # user2 = User(email="patient", password=generate_password_hash("patient", method='sha256'), role="2",
    #              name="benh nhan", ip="127.0.0.1")
    #
    # db.session.add(user)
    # db.session.add(user1)
    # db.session.add(user2)
    # db.session.commit()

    for x in range(20):
        userN = User(email=fake.safe_email(), password=generate_password_hash("patient", method='sha256'), role="2",
                 name=fake.name(), ip="127.0.0.1")
        db.session.add(userN)
        db.session.commit()
    for x in range(20):
        userN = User(email=fake.safe_email(), password=generate_password_hash("doctor", method='sha256'), role="1",
                     name=fake.name())
        db.session.add(userN)
        db.session.commit()



if __name__ == "__main__":
    # app.run(debug=True)
    manager.run()
