import collections
import socket

import sounddevice as sd
import zmq

from config import SAMPLING_RATE, BLOCK_SIZE

host_name = socket.gethostname()
host_ip = socket.gethostbyname(host_name)

audio_queue = collections.deque(maxlen=1000)  # Queue for incoming audio blocks
live_audio_feed = collections.deque(maxlen=1)


def capture_audio(block, block_len, time, status):
    audio_queue.append(block.copy())


def send_array(socket, A, flags=0, copy=True, track=False):
    """send a numpy array with metadata"""
    md = dict(
        dtype=str(A.dtype),
        shape=A.shape,
    )
    socket.send_json(md, flags | zmq.SNDMORE)
    return socket.send(A, flags, copy=copy, track=track)


if __name__ == '__main__':

    context = zmq.Context()
    print("Connecting to hello world server")
    skt = context.socket(zmq.XREQ)
    skt.connect("tcp://localhost:5555")
    stream = sd.InputStream(channels=1, dtype='float32', callback=capture_audio,
                            samplerate=SAMPLING_RATE, blocksize=BLOCK_SIZE)
    stream.start()

    blocks = []
    processing_queue = collections.deque()
    while True:
        skt.send_string(host_ip)
        # message = skt.recv()
        # print("Received reply: %s" % (message))
