
# -*- coding: utf-8 -*-

import threading

import receiver


def on_server_loaded(server_context):
    detector_thread = threading.Thread(target=receiver.stream_init)
    detector_thread.daemon = True
    detector_thread.start()