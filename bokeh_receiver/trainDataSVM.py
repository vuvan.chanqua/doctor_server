
from sklearn.model_selection import train_test_split
from sklearn import svm, metrics
import os


def getData():
    lables = []
    x = []
    features = ["mfcc"]

    pathWav = "/home/tad/Desktop/DA/DataAudio/data_train/"
    filenames = os.listdir(pathWav)

    for filename in filenames:
        filename = filename.split(".")[0]
        print(filename)

        for feature in features:
            path = "/home/tad/Desktop/DA/DataAudio/feature/"
            path += feature
            file = path
            file += "/" + filename + ".csv"
            with open(file) as f:
                lines = f.readlines()

            for line in lines:
                t = []
                a = line.split(",")
                lables.append(a[len(a) - 1].splitlines()[0])
                a = a[:-1]
                for p in a:
                    t.append(round(float(p), 12))
                x.append(t)


    return x,lables


if __name__ == '__main__':

    X, labels = getData()
    X_train, X_test, y_train, y_test = train_test_split(X, labels, test_size=0.30)

    classifier = svm.SVC(kernel = 'rbf',gamma=0.01)
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    print (metrics.classification_report(y_test, y_pred))
