from init import db
from init import login_required
from flask import Blueprint, request, flash, jsonify
from flask_login import current_user
from model.message import Message
from model.appointment import Appointment
from model.user import User
from model.notification import Notification

ajax_patient = Blueprint('Ajax_Patient', __name__, url_prefix="/patient/ajax")


@ajax_patient.route("/read_message/<id>", methods=['GET', 'POST'])
@login_required(role=["2"])
def message_read(id):
    notification = Notification.query.get(id)
    notification.is_read = 1
    db.session.commit()
    ret = {
        'code': 200,
        'message': "Success",
        'data': {
            'id': str(notification.id),
            'body': notification.body
        }
    }
    return ret


@ajax_patient.route("/list_message", methods=["POST", "GET"])
@login_required(role=["2"])
def message_list():
    message = Message.query.filter_by(recipient_id=current_user.id)
    messages = []
    for r in message:
        messages.append({
            'id': r.id,
            'body': r.body,
            'timestamp': r.timestamp
        })
    ret = {
        'code': 200,
        'message': "Success",
        'data': messages
    }
    return ret


@ajax_patient.route("/number_message_unread", methods=["POST", "GET"])
@login_required(role=["1", "2"])
def message_unread_count():
    notification = Notification.query.filter_by(recipient_id=current_user.id, is_read=0)
    total = notification.count()
    ret = {
        'code': 200,
        'message': "Success",
        'data': total
    }
    return ret


@ajax_patient.route("/appointment/delete", methods=['GET', 'POST'])
@login_required(role=["2"])
def patient_appointment_delete():
    result = request.get_json(force=True)
    id = result.get('id')
    appointment = Appointment.query.get(id)
    appointment.status = 0
    appointment.content = result.get('content')
    notification = Notification(sender_id=current_user.id,
                                recipient_id=appointment.doctor_id,
                                object_id=appointment.id,
                                title="Hủy lịch khám",
                                body=result.get('content'),
                                object_type=1)
    db.session.add(notification)

    db.session.commit()
    flash("Xóa thành công", "success")
    ret = {
        'code': 200,
        'message': "Đã hủy lịch khám",
        'status': "Success",
        'data': {
            "id": appointment.id,
            "title": appointment.title
        }
    }
    return jsonify(ret)


@ajax_patient.route("/doctor/list", methods=['GET', 'POST'])
# @login_required(role=["2"])
def doctor_list():
    result = request.get_json(force=True)
    specialist_id = result.get('specialist_id')
    specialist_id = int(specialist_id)
    if specialist_id != 0:
        user = User.query.filter_by(role=1, specialist_id=specialist_id).order_by(User.name)
    else:
        user = User.query.filter_by(role=1).order_by(User.name)
    data = []
    for r in user:
        data.append({
            "id": r.id,
            "name": r.name
        })
    ret = {
        'code': 200,
        'data': data
    }
    return jsonify(ret)


from model.schedule import Schedule


@ajax_patient.route("/list/schedule", methods=["POST", "GET"])
@login_required(role=["2"])
def schedule_patient_choose():
    result = request.get_json(force=True)
    doctor_id = result.get('doctor_id')
    print(doctor_id)
    schedule_list = Schedule.query.filter_by(doctor_id=doctor_id, status=1)
    messages = []
    for r in schedule_list:
        messages.append({
            'id': r.id,
            'time_start': r.time_start,
            'time_finish': r.time_finish,
            'all_day': r.all_day,
            'status': r.status,
            'content': r.content
        })
    ret = {
        'code': 200,
        'message': "Success",
        'data': messages
    }
    return ret
