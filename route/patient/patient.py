from init import db
from init import login_required
from bokeh.embed import autoload_server
from flask import Blueprint, render_template, request, redirect, url_for, flash
from flask_login import current_user
from flask_paginate import Pagination, get_page_args
from model.record_patient import Record
from model.message import Message
from model.notification import Notification

patient = Blueprint('Patient', __name__, url_prefix="/patient")


@patient.route('/')
@login_required(role=["2"])
def patient_index():
    script = autoload_server(model=None, url="http://localhost:5006/bokeh_receiver")
    return render_template("patient/index.html", current_user=current_user, bokS=script)


@patient.route('/assign')
@login_required(role=["2"])
def patient_assign():
    return render_template("patient/assign.html", current_user=current_user)


@patient.route('/chat')
@login_required(role=["2"])
def patient_chat():
    return render_template("patient/chat.html", current_user=current_user)


# @patient.route('/record')
# @login_required(role=["2"])
# def patient_record():
#     return render_template("patient/record.html", current_user=current_user)

# _________________________________________________________________________________________________________RECORD_START__
@patient.route('/record')
@login_required(role=["2"])
def patient_record_list():
    menu = "patient_record"

    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')

    page_size = Record.query.filter_by(id_patient=current_user.id).count()

    if search:
        regex = "%{}%".format(q)
        record = Record.query.filter(Record.name.like(regex), Record.id_patient.like(current_user.id)).limit(
            per_page).offset(offset)
    else:
        record = Record.query.filter_by(id_patient=current_user.id).limit(per_page).offset(offset)

    pagination = Pagination(page=page, total=page_size, record_name='record', css_framework='bootstrap4', search=search)

    return render_template("patient/record.html",
                           current_user=current_user,
                           record=record,
                           search=search,
                           menu=menu,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)


@patient.route('/record/create', methods=['POST', 'GET'])
@login_required(role=["2"])
def patient_record_create():
    is_create = True
    menu = "patient_record"
    result = request.form
    if request.method == "POST":
        content = result.get('content')
        title = result.get('title')
        record = Record(content=content, id_patient=current_user.id, name=title)
        db.session.add(record)
        db.session.commit()
        is_create = False
        flash("Tạo mới thành công", "success")
        # return render_template("patient/record_detail.html", current_user=current_user, menu=menu, is_create=is_create,
        #                        record=record
        #                        )
        return redirect(url_for("Patient.patient_record_edit", id=record.id))
    if request.method == "GET":
        is_create = True
        return render_template("patient/record_detail.html", current_user=current_user, menu=menu, is_create=is_create)


@patient.route('/record/edit/<id>', methods=['POST', 'GET'])
@login_required(role=["2"])
def patient_record_edit(id):
    is_create = False
    menu = "patient_record"
    record = Record.query.filter_by(id=id).first()
    return render_template(
        "patient/record_detail.html",
        current_user=current_user,
        menu=menu,
        record=record,
        is_create=is_create
    )


@patient.route('/record/update', methods=['POST', 'GET'])
@login_required(role=["2"])
def patient_record_update():
    is_create = False
    menu = "patient_record"
    result = request.form
    print(result)
    id = result.get('id')
    title = result.get('title')
    content = result.get('content')

    record = Record.query.get(id)
    record.name = title
    record.content = content
    db.session.commit()

    return render_template(
        "patient/record_detail.html",
        current_user=current_user,
        menu=menu,
        record=record,
        is_create=is_create
    )


@patient.route('/record/delete/<id>', methods=['POST', 'GET'])
@login_required(role=["2"])
def patient_record_delete(id):
    is_create = False
    user = Record.query.filter_by(id=id).first()
    db.session.delete(user)
    db.session.commit()

    return redirect(url_for('Patient.patient_record_list'))


# _________________________________________________________________________________________________________RECORD_END__
@patient.route('/notification', methods=['GET'])
@login_required(role=["1", "2"])
def patient_message():
    menu = 'patient_notification'
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')

    page_size = Notification.query.filter_by(recipient_id=current_user.id).count()

    if search:
        regex = "%{}%".format(q)
        notificatons = Notification.query.filter(Notification.title.like(regex),
                                                 Notification.recipient_id.like(current_user.id)) \
            .limit(per_page).offset(offset)
    else:
        notificatons = Notification.query.filter_by(recipient_id=current_user.id).limit(per_page).offset(offset)

    pagination = Pagination(page=page, total=page_size, record_name='notification', css_framework='bootstrap4',
                            search=search)

    return render_template("patient/notification/message.html",
                           current_user=current_user,
                           notificatons=notificatons,
                           search=search,
                           menu=menu,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)

    # return render_template("patient/message.html", menu=menu, current_user = current_user, )
