from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import current_user
from flask_paginate import Pagination, get_page_args

from config import ROLE_DOCTOR
from form.form import AppointmentForm
from init import db
from init import login_required
from model.appointment import Appointment
from model.specialist import Specialist
from model.record_patient import Record
from model.user import User
from model.schedule import Schedule
from datetime import datetime
import dateutil.parser as dp

patient_appointment = Blueprint('PatientAppointment', __name__, url_prefix="/patient/appointment")


@patient_appointment.route('/')
@login_required(role=["2"])
def index():
    menu = "patient_appointment"
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')

    page_size = Appointment.query.filter_by(patient_id=current_user.id).count()

    if search:
        regex = "%{}%".format(q)
        appointment = Appointment.query.filter(Appointment.title.like(regex),
                                               Record.patient_id.like(current_user.id)).limit(
            per_page).offset(offset)
    else:
        appointment = Appointment.query.filter_by(patient_id=current_user.id).limit(per_page).offset(offset)

    pagination = Pagination(page=page, total=page_size, record_name='record', css_framework='bootstrap4', search=search)

    return render_template("patient/appointment/index.html",
                           current_user=current_user,
                           appointment=appointment,
                           search=search,
                           menu=menu,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)


@patient_appointment.route('/create', methods=["GET", "POST"])
@login_required(role=["2"])
def create():
    is_create = True
    disable = False
    menu = "patient_appointment"
    doctor = User.query.filter_by(role=ROLE_DOCTOR)
    form = AppointmentForm()
    specialist = Specialist.query.all()
    arrr = []
    doctorArr = []
    for r in specialist:
        arrr.append(r)
    for r in doctorArr:
        doctorArr.append(r)
    doctorArr.append(User(id=0, name="Không có giá trị"))
    spe_all = Specialist(id=0, name="Chọn tất cả")
    arrr.append(spe_all)

    form.doctor_select.choices = [(i.id, i.name) for i in doctorArr]
    form.specialist_select.choices = [(i.id, i.name) for i in arrr]

    if request.method == "GET":
        return render_template("patient/appointment/edit.html", disable=disable, current_user=current_user, menu=menu,
                               form=form, is_create=is_create)
    if request.method == "POST":
        result = request.form
        schedule = Schedule.query.get(result.get('schedule_list'))

        appointment = Appointment(doctor_id=result.get('doctor_select'),
                                  patient_id=current_user.id,
                                  time_start=schedule.time_start,
                                  time_finish=schedule.time_start,
                                  all_day="0",
                                  title=result.get('title'))


        flash("Thêm lịch hẹn thành công", "success")
        db.session.add(appointment)
        schedule.status = 0
        db.session.commit()
        return redirect(url_for("PatientAppointment.edit", id=appointment.id))
        # return render_template("patient/appointment/edit.html", current_user=current_user, menu=menu, form=form, is_create=is_create)


@patient_appointment.route('/edit/<id>')
@login_required(role=["2"])
def edit(id):
    is_create = False
    disable = True
    menu = "patient_appointment"
    doctor = User.query.filter_by(role=ROLE_DOCTOR)
    form = AppointmentForm()
    specialist = Specialist.query.all()
    arrr = []
    for r in specialist:
        arrr.append(r)
    spe_all = Specialist(id=0, name="Chọn tất cả")
    arrr.append(spe_all)
    appointment = Appointment.query.get(id)
    form.doctor_select.choices = [(i.id, i.name) for i in doctor]
    form.specialist_select.choices = [(i.id, i.name) for i in arrr]

    if request.method == "GET":
        return render_template("patient/appointment/edit.html",
                               appointment=appointment,
                               current_user=current_user,
                               disable=disable,
                               menu=menu, form=form,
                               is_create=is_create)


@patient_appointment.route('/update', methods=["GET", "POST"])
@login_required(role=["2"])
def update():
    result = request.form
    print(result)
    appointment = Appointment.query.get(result['id'])
    appointment.doctor_id = result['doctor_select']
    appointment.patient_id=current_user.id
    # appointment.time_start = result['time_start']
    # appointment.time_finish = result['time_finish']
    appointment.specialist_id = result.get('specialist_id')
    appointment.title = result['title']
    db.session.commit()

    flash("Cập nhật thành công", "success")

    return redirect(url_for("PatientAppointment.edit", id=appointment.id))
