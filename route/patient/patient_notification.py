from init import db
from init import login_required
from bokeh.embed import autoload_server
from flask import Blueprint, render_template, request, redirect, url_for, flash
from flask_login import current_user
from flask_paginate import Pagination, get_page_args
from model.record_patient import Record
from model.message import Message
from model.user import User
from model.message import Message
from form.form import MessageForm
from sqlalchemy import or_, and_
from model.prescription import Prescription
from form.form import PrescriptionForm
from model.appointment import Appointment

patient_notification = Blueprint('PatientNotification', __name__, url_prefix="/patient/notification")
menu1 = "patient_message"


@patient_notification.route('/message', methods=['GET'])
@login_required(role=["2"])
def index():
    menu = menu1
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')
    data = []
    if search:
        regex = "%{}%".format(q)
        page_size = User.query.filter(and_(User.email.like(regex), or_(User.role == 1, User.role == 0))).count()
        list_user = User.query.filter(and_(User.email.like(regex), or_(User.role == 1, User.role == 0))).limit(
            per_page).offset(offset)
        for r in list_user:
            count_messages = Message.query.filter(
                and_(and_(Message.recipient_id == current_user.id, Message.sender_id == r.id),
                     Message.is_read == 0)).count()
            data.append({
                "id": r.id,
                "email": r.email,
                "name": r.name,
                "message_count": count_messages
            })
    else:
        page_size = User.query.filter(User.role == 1).count()
        list_user = User.query.filter(or_(User.role == 1, User.role == 0)).limit(per_page).offset(offset)

        for r in list_user:
            count_messages = Message.query.filter(
                and_(and_(Message.recipient_id == current_user.id, Message.sender_id == r.id),
                     Message.is_read == 0)).count()
            data.append({
                "id": r.id,
                "email": r.email,
                "name": r.name,
                "message_count": count_messages
            })

    pagination = Pagination(page=page, total=page_size, record_name='list_user', css_framework='bootstrap4',
                            search=search)

    return render_template('patient/notification/index.html',
                           list_user=data,
                           current_user=current_user,
                           menu=menu,
                           search=search,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)

    # return render_template("patient/message.html", menu=menu, current_user = current_user, )


#
@patient_notification.route("/message/<id>")
@login_required(role=["2"])
def message(id):
    menu = menu1
    form = MessageForm()
    patient = User.query.get(id)
    messages = Message.query.filter(or_(and_(Message.recipient_id == id, Message.sender_id == current_user.id),
                                        and_(Message.recipient_id == current_user.id, Message.sender_id == id)))
    update_message = Message.query.filter_by(sender_id=id)
    for r in update_message:
        r.is_read = 1
        db.session.commit()

    return render_template("patient/notification/patient_message.html",
                           menu=menu,
                           current_user=current_user,
                           form=form,
                           patient=patient,
                           messages=messages)


@patient_notification.route("/message", methods=["POST"])
@login_required(role=["2"])
def message_send():
    menu = menu1
    form = MessageForm()
    result = request.form
    id_patient = result.get('id_patient')
    msg = Message(sender_id=current_user.id, recipient_id=id_patient, body=result.get('message'))
    db.session.add(msg)
    db.session.commit()
    patient = User.query.get(id_patient)
    messages = Message.query.filter_by(recipient_id=id_patient)

    # return render_template("admin/patient/patient_message.html", menu=menu, current_user=current_user, form=form,
    #                        patient=patient, messages=messages)
    return redirect(url_for("AdminChat.patient_message", id=id_patient))


@patient_notification.route('/prescription', methods=['GET'])
@login_required(role=["2"])
def prescription():
    menu = menu1
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')
    if search:
        regex = "%{}%".format(q)
        page_size = Prescription.query.filter(Prescription.body.like(regex),
                                              Prescription.patient_id == current_user.id).count()
        prescription = Prescription.query.filter(Prescription.body.like(regex),
                                                 Prescription.patient_id == current_user.id).limit(per_page).offset(
            offset)
    else:
        page_size = Prescription.query.filter(Prescription.patient_id == current_user.id).count()
        prescription = Prescription.query.filter(Prescription.patient_id == current_user.id).limit(per_page).offset(
            offset)

    pagination = Pagination(page=page, total=page_size, record_name='prescription', css_framework='bootstrap4',
                            search=search)

    return render_template('patient/notification/prescription.html',
                           prescription=prescription,
                           current_user=current_user,
                           menu='patient_prescription',
                           search=search,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)


@patient_notification.route('/<id>', methods=['POST', 'GET'])
@login_required(role=["2"])
def edit(id):
    is_create = False
    patient = User.query.get(current_user.id)
    prescription = Prescription.query.get(id)

    appointment = Appointment.query.filter_by(patient_id=current_user.id)
    return render_template('patient/notification/prescription_detail.html', current_user=current_user, menu='patient_prescription',
                           patient=patient, appointment=appointment, prescription=prescription, is_create=is_create)
