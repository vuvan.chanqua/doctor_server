from flask import Blueprint, jsonify
from flask import flash, request

from init import db
from init import login_required
from model.appointment import Appointment
from model.message import Message
from sqlalchemy import or_, and_
from flask_login import current_user
from model.notification import Notification

ajax_admin = Blueprint('Ajax_Admin', __name__, url_prefix="/admin/ajax")


@ajax_admin.route("/appointment/list/<id>", methods=['GET', 'POST'])
# @login_required(role=["2"])
def appointment_list(id):
    appointment = Appointment.query.filter_by(doctor_id=id)
    result = []
    for r in appointment:
        if r.all_day == '1':
            all_day = True
        else:
            all_day = False
        if r.status == 1:
            result.append({
                'id': r.id,
                'title': r.title,
                'start': r.time_start,
                'end': r.time_finish,
                'allDay': all_day,
            })
        else:
            result.append({
                'id': r.id,
                'title': r.title,
                'start': r.time_start,
                'end': r.time_finish,
                "color": '#FF0000',
                'allDay': all_day,
            })
    # ret = {
    #     'code': 200,
    #     'message': "Success",
    #     'data': result
    # }
    return jsonify(result)


@ajax_admin.route("/appointment/delete", methods=['GET', 'POST'])
@login_required(role=["0", "1"])
def appointment_delete():
    result = request.get_json(force=True)
    id = result.get('id')
    appointment = Appointment.query.get(id)
    appointment.status = 0
    appointment.content = result.get('content')

    notification = Notification(sender_id=current_user.id,
                                recipient_id=appointment.patient_id,
                                object_id=appointment.id,
                                title="Hủy lịch khám",
                                body=result.get('content'),
                                object_type=1)
    db.session.add(notification)

    db.session.commit()

    ret = {
        'code': 200,
        'message': "Đã hủy lịch khám",
        'status': "Success",
        'data': {
            "id": appointment.id,
            "title": appointment.title
        }
    }
    return jsonify(ret)


@ajax_admin.route("/appointment/change", methods=['GET', 'POST'])
@login_required(role=["0", "1"])
def appointment_change():
    result = request.get_json(force=True)
    id = result.get('id')
    appointment = Appointment.query.get(id)
    appointment.time_start = result.get('time_start')
    appointment.time_finish = result.get('time_finish')
    appointment.content = result.get('content')
    print(appointment.patient_id)
    notification = Notification(sender_id=current_user.id,
                                recipient_id=appointment.patient_id,
                                object_id=appointment.id,
                                title="Đổi lịch khám",
                                body=result.get('content'),
                                object_type=1)
    db.session.add(notification)
    db.session.commit()
    ret = {
        'code': 200,
        'message': "Đổi lịch khám thành công",
        'status': "Success"
    }
    return jsonify(ret)


@ajax_admin.route("/appointment/offset", methods=['GET', 'POST'])
@login_required(role=["0", "1"])
def appointment_offset():
    result = request.get_json(force=True)
    doctor_id = result.get('doctor_id')
    time = result.get('time')
    allday = result.get('allday')
    title = result.get('title')
    appointment = Appointment(doctor_id=doctor_id, time_start=time, time_finish=time, all_day=allday, title=title)
    notification = Notification(sender_id=current_user.id,
                                recipient_id=appointment.patient_id,
                                object_id=appointment.id,
                                title="Đổi lịch khám",
                                body=result.get('content'),
                                object_type=1)
    db.session.add(notification)
    db.session.add(appointment)
    db.session.commit()
    ret = {
        'code': 200,
        'message': "Đổi lịch khám thành công",
        'status': "Success"

    }
    return jsonify(ret)


@ajax_admin.route("/message/list/<patient_id>", methods=['GET', 'POST'])
# @login_required(role=["0", "1"])
def chat_list(patient_id):
    # messages = Message.query.filter(or_(Message.sender_id == current_user.id, Message.recipient_id == current_user.id))
    messages = Message.query.filter(or_(and_(Message.recipient_id == patient_id, Message.sender_id == current_user.id),
                                        and_(Message.recipient_id == current_user.id, Message.sender_id == patient_id)))
    data = []
    for r in messages:
        # item['sender_id'] == current_user.id
        if r.sender_id == current_user.id:
            data.append({
                'sender_id': r.sender_id,
                'recipient_id': r.recipient_id,
                'body': '<li class ="text-right mr-3"> ' + r.body + '</li>'
                # {{item['body']}}
            })
        else:
            data.append({
                'sender_id': r.sender_id,
                'recipient_id': r.recipient_id,
                'body': '<li class ="text-left"> ' + r.body + '</li>'
            })
    ret = {
        'code': 200,
        'data': data,
    }
    return jsonify(ret)


# ____________________________________________________________________________________________________________
from model.schedule import Schedule


@ajax_admin.route("/schedule/list/<id>", methods=['GET', 'POST'])
# @login_required(role=["2"])
def schedule_list(id):
    schedule = Schedule.query.filter_by(doctor_id=id)
    result = []
    for r in schedule:
        if r.all_day == '1':
            all_day = True
        else:
            all_day = False
        if r.status == 1:
            result.append({
                'id': r.id,
                'title': r.title,
                'start': r.time_start,
                'end': r.time_finish,
                'allDay': all_day,
            })
        else:
            result.append({
                'id': r.id,
                'title': r.title,
                'start': r.time_start,
                'end': r.time_finish,
                "color": '#FF0000',
                'allDay': all_day,
            })
    return jsonify(result)


@ajax_admin.route("/schedule/delete", methods=['GET', 'POST'])
@login_required(role=["0", "1"])
def schedule_delete():
    result = request.get_json(force=True)
    id = result.get('id')
    schedule = Schedule.query.get(id)
    schedule.status = 0
    schedule.content = result.get('content')
    db.session.commit()

    ret = {
        'code': 200,
        'message': "Đã hủy lịch khám",
        'status': "Success",
        'data': {
            "id": schedule.id,
            "title": schedule.title
        }
    }
    return jsonify(ret)


@ajax_admin.route("/schedule/change", methods=['GET', 'POST'])
@login_required(role=["0", "1"])
def schedule_change():
    result = request.get_json(force=True)
    id = result.get('id')
    schedule = Schedule.query.get(id)
    schedule.time_start = result.get('time_start')
    schedule.time_finish = result.get('time_finish')
    schedule.content = result.get('content')
    db.session.commit()
    ret = {
        'code': 200,
        'message': "Đổi lịch khám thành công",
        'status': "Success"
    }
    return jsonify(ret)


@ajax_admin.route("/schedule/offset", methods=['GET', 'POST'])
@login_required(role=["0", "1"])
def schedule_offset():
    result = request.get_json(force=True)
    doctor_id = result.get('doctor_id')
    time = result.get('time')
    allday = result.get('allday')
    title = result.get('title')
    schedule = Schedule(doctor_id=doctor_id, time_start=time, time_finish=time, all_day=allday, title=title)
    db.session.add(schedule)
    db.session.commit()
    ret = {
        'code': 200,
        'message': "Đổi lịch khám thành công",
        'status': "Success"

    }
    return jsonify(ret)
