from init import db
from init import login_required
from bokeh.embed import autoload_server
from flask import Blueprint, render_template, redirect, url_for, request, flash, make_response
from flask_login import current_user
from flask_paginate import Pagination, get_page_args
from form.form import UserForm, MessageForm
from model.message import Message
from model.record_patient import Record
from model.user import User
from sqlalchemy import and_
from werkzeug.security import generate_password_hash
from config import MENU_ADMIN_CHAT, MENU_ADMIN_PATIENT
from sqlalchemy import or_, and_

admin_chat = Blueprint('AdminChat', __name__, url_prefix="/admin/chat")
@admin_chat.route('/', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def chat():
    menu = MENU_ADMIN_CHAT
    return render_template('admin/chat/chat.html',current_user=current_user, menu=menu)

@admin_chat.route("/message/<id>")
@login_required(role=["0", "1", "2"])
def patient_message(id):
    menu = MENU_ADMIN_PATIENT
    form = MessageForm()
    patient = User.query.get(id)
    messages = Message.query.filter(or_(and_(Message.recipient_id==id, Message.sender_id==current_user.id),
                                        and_(Message.recipient_id==current_user.id, Message.sender_id==id)))
    # if request.method == "GET":
    return render_template("admin/patient/patient_message.html",
                           menu=menu,
                           current_user=current_user,
                           form=form,
                           patient=patient,
                           messages=messages)


@admin_chat.route("/message", methods=["POST"])
@login_required(role=["0", "1", "2"])
def patient_message_send():
    menu = MENU_ADMIN_PATIENT
    form = MessageForm()
    result = request.form
    id_patient = result.get('id_patient')
    msg = Message(sender_id=current_user.id, recipient_id=id_patient, body=result.get('message'))
    db.session.add(msg)
    db.session.commit()
    patient = User.query.get(id_patient)
    messages = Message.query.filter_by(recipient_id=id_patient)

    # return render_template("admin/patient/patient_message.html", menu=menu, current_user=current_user, form=form,
    #                        patient=patient, messages=messages)
    return redirect(url_for("AdminChat.patient_message", id=id_patient))
