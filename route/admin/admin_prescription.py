from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import current_user

from config import MENU_ADMIN_PATIENT
from form.form import PrescriptionForm
from init import db
from init import login_required
from model.appointment import Appointment
from model.prescription import Prescription
from model.user import User

menu = MENU_ADMIN_PATIENT

admin_prescription = Blueprint('AdminPrescription', __name__, url_prefix="/admin/prescription")


@admin_prescription.route('/create/<id>', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def create(id):
    is_create = True
    patient = User.query.get(id)
    form = PrescriptionForm(request.form)
    appointment = Appointment.query.filter_by(patient_id=id)
    # form.appointment_id.choices = [(r.id, r.time_start +" - " + r.time_finish) for r in appointment]
    if request.method == "GET":
        return render_template('admin/patient/prescription.html', current_user=current_user, menu=menu, form=form,
                               patient=patient, appointment=appointment, is_create=is_create)
    if request.method == "POST":
        if form.validate():
            result = request.form
            body = result.get("body")
            drug = result.get("drug")
            diagnose = result.get("diagnose")
            appointment_id = result.get("appointment_id")
            prescription = Prescription(doctor_id=current_user.id, patient_id=id, body=body, drug=drug, diagnose=diagnose,
                                        appointment_id=appointment_id)
            db.session.add(prescription)
            db.session.commit()
            flash("Thêm mới thành công", "success")
            # return render_template('admin/patient/prescription.html', current_user=current_user, menu=menu, form=form,
            #                    patient=patient, appointment=appointment)
            return redirect(url_for("AdminPrescription.edit", patient_id = id,prescription_id=prescription.id))
        else:
            flash("Thêm mới thất bạt", "error")
            return render_template('admin/patient/prescription.html', current_user=current_user, menu=menu, form=form,
                                   patient=patient, appointment=appointment, is_create=is_create)


@admin_prescription.route('/<patient_id>/<prescription_id>', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def edit(patient_id, prescription_id):
    is_create = False

    patient = User.query.get(patient_id)
    prescription = Prescription.query.get(prescription_id)
    form = PrescriptionForm(request.form)

    appointment = Appointment.query.filter_by(patient_id=patient_id)

    form.body.data = prescription.body
    form.drug.data = prescription.drug
    form.diagnose.data = prescription.diagnose

    return render_template('admin/patient/prescription.html', current_user=current_user, menu=menu, form=form,
                           patient=patient, appointment=appointment, prescription=prescription, is_create=is_create)


@admin_prescription.route('/update/<patient_id>/<prescription_id>', methods=['POST'])
@login_required(role=["0", "1"])
def update(patient_id, prescription_id):
    is_create = False

    patient = User.query.get(patient_id)

    form = PrescriptionForm(request.form)

    appointment = Appointment.query.filter_by(patient_id=patient_id)
    if form.validate():
        result = request.form

        body = result.get("body")
        appointment_id = result.get("appointment_id")
        prescription = Prescription.query.get(prescription_id)

        prescription.diagnose = result.get("diagnose")
        prescription.drug = result.get("drug")
        prescription.body = body
        prescription.appointment_id = appointment_id
        db.session.commit()
        flash("Cập nhật thành công", "success")
        return redirect(url_for("AdminPrescription.edit", patient_id=patient_id, prescription_id=prescription_id))
    flash("Cập nhật thất bại", "error")
    return redirect(url_for("AdminPrescription.edit", patient_id=patient_id, prescription_id=prescription_id))


@admin_prescription.route('/delete/<patient_id>/<id>', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def delete(patient_id, id):
    prescription = Prescription.query.get(id)
    db.session.delete(prescription)
    db.session.commit()
    flash("Xóa thành công", "success")
    return redirect(url_for('User.patient_detail', id=patient_id))