from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask_login import current_user
from flask_paginate import Pagination, get_page_args

from config import MENU_ADMIN_SPECAILIST
from form.form import SpecialistForm
from init import db
from init import login_required
from model.specialist import Specialist

menu = MENU_ADMIN_SPECAILIST

admin_specialist = Blueprint('AdminSpecialist', __name__, url_prefix="/admin/specialist")


@admin_specialist.route('/', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def list():
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')

    page_size = Specialist.query.filter_by().count()
    if search:
        regex = "%{}%".format(q)
        specialist = Specialist.query.filter().limit(per_page).offset(offset)
    else:
        specialist = Specialist.query.filter().limit(per_page).offset(offset)

    pagination = Pagination(page=page, total=page_size, record_name='list_user', css_framework='bootstrap4',
                            search=search)

    return render_template('admin/specialist/index.html',
                           specialist=specialist,
                           current_user=current_user,
                           menu=menu,
                           search=search,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)


@admin_specialist.route('/create', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def create():
    is_create = True
    form = SpecialistForm(request.form)
    result = request.form
    name = result.get('name')
    specialist = None
    if request.method == "GET":
        return render_template("admin/specialist/edit.html", menu=menu, current_user=current_user, form=form,
                               is_create=is_create, specialist=specialist)
    if request.method == "POST":
        if form.validate():
            specialist = Specialist(name=name)
            db.session.add(specialist)
            db.session.commit()
            flash("Tạo mới thành công", 'success')
            return redirect(url_for("AdminSpecialist.edit", id=specialist.id))
        flash("Tạo mới thật bại", 'error')
        return redirect(url_for("AdminSpecialist.edit", id=specialist.id))


@admin_specialist.route('/edit/<id>', methods=['GET'])
@login_required(role=["0", "1"])
def edit(id):
    is_create = False
    form = SpecialistForm(request.form)
    result = request.form
    name = result.get('name')
    specialist = Specialist.query.get(id)
    return render_template("admin/specialist/edit.html", menu=menu, current_user=current_user, form=form,
                           is_create=is_create, specialist=specialist)


@admin_specialist.route('/update', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def update():
    form = SpecialistForm(request.form)
    result = request.form
    is_create = False
    print(result)
    id = result.get('id')
    name = result.get('name')

    if form.validate():
        specialist = Specialist.query.get(id)
        specialist.name = name
        db.session.commit()
        flash("Cập nhật thành công", 'success')
        return redirect(url_for("AdminSpecialist.edit", id=id))
    else:
        flash("Cập nhật thất bại", 'error')
        return redirect(url_for("AdminSpecialist.edit", id=id))


@admin_specialist.route('/delete/<id>', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def delete(id):
    specialist = Specialist.query.get(id)
    db.session.delete(specialist)
    db.session.commit()
    flash("Xóa thành công", "success")
    return redirect(url_for('AdminSpecialist.list'))
