from bokeh.embed import autoload_server
from flask import Blueprint, render_template, redirect, url_for, request, flash, make_response
from flask_login import current_user
from flask_paginate import Pagination, get_page_args
from sqlalchemy import and_
from werkzeug.security import generate_password_hash

from config import MENU_ADMIN_PATIENT, MENU_ADMIN_USER
from form.form import UserForm, MessageForm
from init import db
from init import login_required
from model.clinic import Clinic
from model.specialist import Specialist
from model.message import Message
from model.record_patient import Record
from model.prescription import Prescription
from model.user import User
from model.gender import Gender
user = Blueprint('User', __name__, url_prefix="/admin")


@user.route('/list', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def list():
    menu = MENU_ADMIN_USER
    # users = User.query.all()
    # list_user = []
    # for r in users:
    #     for ip in list_ip:
    #         user_role = int(r.role)
    #         current_user_role = int(current_user.role)
    #         if current_user_role < user_role or current_user.role == '0':
    #             if ip == r.ip:
    #                 list_user.append({
    #                     'id': r.id,
    #                     'ip': r.ip,
    #                     'username': r.email,
    #                     'name': r.name,
    #                     'role': r.role,
    #                     'active': 1
    #                 })
    #             else:
    #                 list_user.append({
    #                     'id': r.id,
    #                     'ip': r.ip,
    #                     'username': r.email,
    #                     'name': r.name,
    #                     'role': r.role,
    #                     'active': 0
    #                 })

    # for ip in list_ip:
    #     for user in list_user:
    #         if str(user['ip']) == str(ip):
    #             print(ip)
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')

    page_size = User.query.filter_by().count()
    if search:
        regex = "%{}%".format(q)
        list_user = User.query.filter(User.email.like(regex)).limit(per_page).offset(offset)
    else:
        list_user = User.query.filter(User.role > current_user.role).limit(per_page).offset(offset)

    pagination = Pagination(page=page, total=page_size, record_name='list_user', css_framework='bootstrap4',
                            search=search)

    response = make_response(render_template('admin/account/list.html',
                                             list_user=list_user,
                                             current_user=current_user,
                                             menu=menu,
                                             search=search,
                                             page=page,
                                             per_page=per_page,
                                             pagination=pagination))
    response.headers['Content-Security-Policy'] = "default-src 'self'"
    response.headers['Strict-Transport-Security'] = 'max-age=31536000; includeSubDomains'
    response.headers['X-Content-Type-Options'] = 'nosniff'

    return response


# @user.route('/')
# @requires_user
# def redirect_to_bokeh():
#     s_id = session_id.generate_session_id()
#     # s_id = "2LjbXmrZe1tcNvqIcoWWuPrtTU7xPDGDtu03rGxwUfK8"
#     return redirect("http://127.0.0.1:5006/?bokeh-session-id={}".format(s_id), code=302)


@user.route('/detail/<id>')
@login_required(role=["0", "1"])
def detail(id):
    # s_id = session_id.generate_session_id()
    # s_id = "2LjbXmrZe1tcNvqIcoWWuPrtTU7xPDGDtu03rGxwUfK8"
    # return redirect("http://127.0.0.1:5006/?bokeh-session-id={}".format(s_id), code=302)
    # return redirect("http://127.0.0.1:5006/?bokeh-session-id=%s" % s_id)
    user = User.query.get(id)
    ip = user.ip

    script = autoload_server(model=None, url="http://" + ip + ":5006/bokeh_receiver")
    return render_template('admin/account/detail.html', bokS=script)


@user.route('/delete/<id>')
@login_required(role=["0", "1"])
def delete(id):
    user = User.query.filter_by(id=id).first()
    flash("Xóa thành công", 'success')
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('User.list'))


@user.route('/create')
@login_required(role=["0", "1"])
def create():
    form = UserForm(request.form)
    genderArr= []
    genderArr.append(Gender(id=1, name="Nam"))
    genderArr.append(Gender(id=2, name="Nữ"))

    form.gender_select.choices = [(i.id, i.name) for i in genderArr]
    menu = MENU_ADMIN_USER
    is_create = True
    clinic = Clinic.query.all()
    specialist = Specialist.query.all()
    user = None
    return render_template('admin/account/edit.html', is_create=is_create, user=user, current_user=current_user, menu=menu,
                           form=form, clinic=clinic, specialist = specialist)


@user.route('/update', methods=['POST'])
@login_required(role=["0", "1"])
def update():
    form = UserForm(request.form)
    clinic = Clinic.query.all()
    result = request.form
    id = result.get('id')
    email = result.get('email')
    # password = result.get('password')
    ip = result.get('ip')
    name = result.get('name')
    role = result.get('role')
    id_card = result.get('id_card')
    address = result.get('address')
    phone_number = result.get('phone_number')
    clinic_id = result.get('clinic_id')
    image_id = result.get('image_id')
    gender = result.get('gender')
    age = result.get('age')

    print(image_id)
    specialist_id = result.get('specialist_id')
    print(image_id)
    is_create = False
    if form.validate():
        user = User.query.get(id)
        user.email = email
        user.ip = ip
        user.name = name
        user.role = role
        user.id_card = id_card
        user.address = address
        user.phone_number = phone_number
        user.clinic_id = clinic_id
        user.specialist_id = specialist_id
        user.image_id = 12

        db.session.commit()

        flash("Cập nhật thành công", 'success')
        return redirect(url_for("User.edit", id=id))
        # return render_template('admin/account/edit.html', is_create=is_create, user=return_user, current_user=current_user,
        #                        form=form, clinic=clinic)
    else:
        flash("Đã xảy ra lỗi", "warning")
        return redirect(url_for("User.edit", id=id))


@user.route('/save', methods=['POST'])
@login_required(role=["0", "1"])
def save():
    form = UserForm(request.form)
    menu = MENU_ADMIN_USER
    is_create = False
    if form.validate():
        result = request.form
        email = result.get('email')
        password = result.get('password')
        ip = result.get('ip')
        name = result.get('name')
        role = result.get('role')
        clinic_id = result.get('clinic_id')
        specialist_id = result.get('specialist_id')
        image_id = result.get('image_id')

        id_card = result.get('id_card')
        address = result.get('address')
        phone_number = result.get('phone_number')

        hash_pass = generate_password_hash(password, method='sha256')
        user = User(email=email, password=hash_pass, ip=ip, role=role, name=name, id_card=id_card, address=address,
                    phone_number=phone_number, clinic_id=clinic_id, image_id=image_id,specialist_id=specialist_id)
        db.session.add(user)
        db.session.commit()

        # return_user = {
        #     'id': user.id,
        #     'ip': user.ip,
        #     'username': user.email,
        #     'name': user.name,
        #     'role': user.role,
        #     'clinic_id': user.clinic_id
        # }

        flash("Tạo mới thành công", 'success')
        # return render_template('admin/account/edit.html', is_create=is_create, user=user,
        #                        current_user=current_user,
        #                        menu=menu, form=form)
        return redirect(url_for("User.edit", id =user.id))


@user.route('/edit/<id>')
@login_required(role=["0", "1"])
def edit(id):
    form = UserForm(request.form)
    is_create = False
    user = User.query.filter_by(id=id).first()
    clinic = Clinic.query.all()
    specialist = Specialist.query.all()
    return render_template('admin/account/edit.html', is_create=is_create, user=user, current_user=current_user,
                           form=form, clinic=clinic, specialist=specialist)


@user.route('/patient')
@login_required(role=["0", "1"])
def patient_list():
    menu = MENU_ADMIN_PATIENT
    # users = User.query.all()
    # list_user = []
    # for r in users:
    #     for ip in list_ip:
    #         user_role = int(r.role)
    #         if user_role == 2:
    #             if ip == r.ip:
    #                 list_user.append({
    #                     'id': r.id,
    #                     'ip': r.ip,
    #                     'username': r.email,
    #                     'name': r.name,
    #                     'role': r.role,
    #                     'active': 1
    #                 })
    #             else:
    #                 list_user.append({
    #                     'id': r.id,
    #                     'ip': r.ip,
    #                     'username': r.email,
    #                     'name': r.name,
    #                     'role': r.role,
    #                     'active': 0
    #                 })
    #
    # return render_template('admin/patient/patient_list.html', list_user=list_user, current_user=current_user, menu=menu)
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')

    page_size = User.query.filter_by(role=2).count()
    if search:
        regex = "%{}%".format(q)
        list_user = User.query.filter(and_(User.email.like(regex), User.role == 2)).limit(per_page).offset(offset)
    else:
        list_user = User.query.filter(User.role == 2).limit(per_page).offset(offset)

    pagination = Pagination(page=page, total=page_size, record_name='list_user', css_framework='bootstrap4',
                            search=search)

    return render_template('admin/patient/patient_list.html',
                           list_user=list_user,
                           current_user=current_user,
                           menu=menu,
                           search=search,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)


@user.route('/patient/detail/<id>')
@login_required(role=["0", "1"])
def patient_detail(id):
    menu = MENU_ADMIN_PATIENT
    record = Record.query.filter_by(id_patient=id)
    prescription = Prescription.query.filter_by(patient_id=id)
    patient = User.query.get(id)
    # search = False
    # q = request.args.get('search')
    # if q:
    #     search = True
    #
    # page, per_page, offset = get_page_args(page_parameter='page',
    #                                        per_page_parameter='per_page')
    #
    # page_size = Record.query.filter_by(id_patient=id).count()
    # # record = Record.query.filter_by(id_patient=id)
    # if search:
    #     regex = "%{}%".format(q)
    #     record = Record.query.filter(Record.title.like(regex), Record.id_patient.like(id)).limit(per_page).offset(
    #         offset)
    # else:
    #     record = Record.query.filter_by(id_patient=id).limit(per_page).offset(offset)
    #
    # pagination = Pagination(page=page, total=page_size, record_name='record', css_framework='bootstrap4', search=search)

    # return render_template("admin/patient/patient_detail.html",
    #                        current_user=current_user,
    #                        record=record,
    #                        search=search,
    #                        menu=menu,
    #                        page=page,
    #                        per_page=per_page,
    #                        pagination=pagination)
    return render_template("admin/patient/patient_detail.html",
                           current_user=current_user, patient=patient, prescription=prescription,
                           record=record, menu=menu)