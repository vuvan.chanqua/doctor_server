from init import db
from init import login_required
from bokeh.embed import autoload_server
from flask import Blueprint, render_template, redirect, url_for, request, flash, make_response
from flask_login import current_user
from flask_paginate import Pagination, get_page_args
from form.form import UserForm, MessageForm
from model.message import Message
from model.record_patient import Record
from model.user import User
from sqlalchemy import and_
from werkzeug.security import generate_password_hash
from config import MENU_ADMIN_DOCTOR

admin_doctor = Blueprint('AdminDoctor', __name__, url_prefix="/admin/doctor")


@admin_doctor.route('/', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def list_doctor():
    menu = MENU_ADMIN_DOCTOR
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')

    page_size = User.query.filter_by(role=1).count()
    if search:
        regex = "%{}%".format(q)
        list_user = User.query.filter(and_(User.email.like(regex), User.role == 1)).limit(per_page).offset(offset)
    else:
        list_user = User.query.filter(User.role == 1).limit(per_page).offset(offset)

    pagination = Pagination(page=page, total=page_size, record_name='list_user', css_framework='bootstrap4',
                            search=search)

    return render_template('admin/doctor/list.html',
                           list_user=list_user,
                           current_user=current_user,
                           menu=menu,
                           search=search,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)


@admin_doctor.route('/appointment/<id>', methods=['GET'])
@login_required(role=["0", "1"])
def edit_calendar(id):
    menu = MENU_ADMIN_DOCTOR
    doctor = User.query.get(id)
    return render_template('admin/doctor/calendar.html', menu=menu, current_user=current_user, doctor=doctor)


@admin_doctor.route('/schedule/<id>', methods=['GET'])
@login_required(role=["0", "1"])
def edit_schedule(id):
    menu = 'user_doctor_schedule'
    doctor = User.query.get(id)
    return render_template('admin/doctor/schedule.html', menu=menu, current_user=current_user, doctor=doctor)


@admin_doctor.route('/update', methods=['POST'])
@login_required(role=["0", "1"])
def update_doctor():
    return 1


@admin_doctor.route('/save', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def save_doctor():
    return 1
