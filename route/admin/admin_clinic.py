from init import db
from init import login_required
from bokeh.embed import autoload_server
from flask import Blueprint, render_template, redirect, url_for, request, flash, make_response
from flask_login import current_user
from flask_paginate import Pagination, get_page_args
from form.form import UserForm, MessageForm
from model.message import Message
from model.record_patient import Record
from model.user import User
from sqlalchemy import and_
from werkzeug.security import generate_password_hash
from config import MENU_ADMIN_DOCTOR
from model.clinic import Clinic
from form.form import ClinicForm

menu = "admin_clinic"

admin_clinic = Blueprint('AdminClinic', __name__, url_prefix="/admin/clinic")


@admin_clinic.route('/', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def list():
    search = False
    q = request.args.get('search')
    if q:
        search = True

    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')

    page_size = Clinic.query.filter_by().count()
    if search:
        regex = "%{}%".format(q)
        clinic = Clinic.query.filter().limit(per_page).offset(offset)
    else:
        clinic = Clinic.query.filter().limit(per_page).offset(offset)

    pagination = Pagination(page=page, total=page_size, record_name='list_user', css_framework='bootstrap4',
                            search=search)

    return render_template('admin/clinic/index.html',
                           clinic=clinic,
                           current_user=current_user,
                           menu=menu,
                           search=search,
                           page=page,
                           per_page=per_page,
                           pagination=pagination)


@admin_clinic.route('/create', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def create():
    is_create = True
    form = ClinicForm(request.form)
    result = request.form
    name = result.get('name')
    address = result.get('address')
    image_id = result.get('image_id')
    description = result.get('description')
    clinic = None
    if request.method == "GET":
        return render_template("admin/clinic/edit.html", menu=menu, current_user=current_user, form=form,
                               is_create=is_create, clinic=clinic)
    if request.method == "POST":
        if form.validate():
            clinic = Clinic(name=name, address=address, description=description)
            db.session.add(clinic)
            db.session.commit()
            flash("Tạo mới thành công", 'success')
            return redirect(url_for("AdminClinic.edit", id=clinic.id))
        flash("Tạo mới thật bại", 'error')
        return redirect(url_for("AdminClinic.edit"))


@admin_clinic.route('/edit/<id>', methods=['GET'])
@login_required(role=["0", "1"])
def edit(id):
    is_create = False
    form = ClinicForm(request.form)
    result = request.form
    name = result.get('name')
    address = result.get('address')
    image_id = result.get('image_id')
    clinic = Clinic.query.get(id)
    return render_template("admin/clinic/edit.html", menu=menu, current_user=current_user, form=form,
                           is_create=is_create, clinic=clinic)


@admin_clinic.route('/update', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def update():
    form = ClinicForm(request.form)
    result = request.form
    is_create = False
    print(result)
    id = result.get('id')
    name = result.get('name')
    address = result.get('address')
    image_id = result.get('image_id')
    description = result.get('description')

    if form.validate():
        clinic = Clinic.query.get(id)
        clinic.name = name
        clinic.address = address
        clinic.description = description
        db.session.commit()
        flash("Cập nhật thành công", 'success')
        return redirect(url_for("AdminClinic.edit", id=id))
    else:
        flash("Cập nhật thất bại", 'error')
        return redirect(url_for("AdminClinic.edit", id=id))


@admin_clinic.route('/delete/<id>', methods=['POST', 'GET'])
@login_required(role=["0", "1"])
def delete(id):
    clinic = Clinic.query.get(id)
    db.session.delete(clinic)
    db.session.commit()
    flash("Xóa thành công", "success")
    return redirect(url_for('AdminClinic.list'))
