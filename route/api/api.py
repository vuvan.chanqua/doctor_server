from flask import Blueprint, render_template, redirect, request, flash, url_for, make_response, jsonify
from model.user import User
from werkzeug.security import check_password_hash, generate_password_hash
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token, create_refresh_token, jwt_refresh_token_required, get_raw_jwt,
    get_jwt_identity
)
from init import blacklist

api = Blueprint('Api', __name__, url_prefix="/api/v1")


@api.route('/login', methods=['POST', 'GET'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    user = User.query.filter_by(email=username).first()
    if user:
        if check_password_hash(user.password, password):
            user_data = {
                "username": user.email,
                "password": user.password,
                "role": user.role
            }
            access_token = create_access_token(identity=user_data)
            refresh_token = create_refresh_token(identity=user_data)
            data = {
                "access_token": access_token,
                "refresh_token": refresh_token
            }
            return jsonify(data=data), 200
        return jsonify({"msg": "Wrong parameter"}), 400

    return jsonify({"msg": "Wrong parameter"}), 400


@api.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    ret = {
        'access_token': create_access_token(identity=current_user)
    }
    return jsonify(ret), 200

# Endpoint for revoking the current users access token
@api.route('/logout', methods=['DELETE'])
@jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200


# Endpoint for revoking the current users refresh token
@api.route('/logout2', methods=['DELETE'])
@jwt_refresh_token_required
def logout2():
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200


@api.route('/protected', methods=['GET'])
@jwt_required
def protected():
    return jsonify({'hello': 'world'})