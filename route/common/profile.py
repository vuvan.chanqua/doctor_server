from init import db
from init import login_required
from bokeh.embed import autoload_server
from flask import Blueprint, render_template, redirect, url_for, request, flash, make_response
from flask_login import current_user
from flask_paginate import Pagination, get_page_args
from form.form import UserForm, MessageForm
from model.message import Message
from model.user import User
from sqlalchemy import and_
from werkzeug.security import generate_password_hash
from config import MENU_ADMIN_PATIENT, MENU_ADMIN_USER
from model.gender import Gender
profile = Blueprint('Profile', __name__, url_prefix="/profile")


@profile.route('/edit', methods=["GET", "POST"])
@login_required(role=["0", "1", "2"])
def edit():
    form = UserForm(request.form)
    genderArr= []
    genderArr.append(Gender(id=1, name="Nam"))
    genderArr.append(Gender(id=2, name="Nữ"))

    form.gender_select.choices = [(i.id, i.name) for i in genderArr]
    result = request.form
    id = result.get('id')
    email = result.get('email')
    # password = result.get('password')
    ip = result.get('ip')
    name = result.get('name')
    id_card = result.get('id_card')
    address = result.get('address')
    phone_number = result.get('phone_number')
    image_id = result.get('image_id')

    if request.method == "GET":
        user = User.query.filter_by(id=current_user.id).first()
        # user = {
        #     'id': r.id,
        #     'ip': r.ip,
        #     'username': r.email,
        #     'name': r.name,
        #     'image_id': r.image_id,
        #     'address': r.address,
        #     'id_card': r.id_card,
        #     'phone_number': r.phone_number,
        #     'password': r.password,
        #     'confirm': r.password
        # }
        return render_template("profile.html", current_user=current_user, form=form, user=user)
    if request.method == "POST":
        if form.validate():
            user = User.query.get(id)
            user.email = email
            user.ip = ip
            user.name = name
            user.image_id = image_id
            user.id_card = id_card
            user.address = address
            user.phone_number = phone_number

            db.session.commit()

            # return_user = {
            #     'id': user.id,
            #     'ip': user.ip,
            #     'username': user.email,
            #     'name': user.name,
            #     'image_id': user.image_id,
            #     'address': user.address,
            #     'id_card': user.id_card,
            #     'phone_number': user.phone_number,
            # }
            flash("Cập nhật thành công", 'success')
            return render_template('profile.html',
                                   user=user,
                                   current_user=current_user,
                                   form=form)
