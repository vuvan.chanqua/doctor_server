from init import db
from init import login_required
from bokeh.embed import autoload_server
from flask import Blueprint, render_template, redirect, url_for, request, flash, make_response, send_from_directory, send_file
from flask_login import current_user
from flask_paginate import Pagination, get_page_args
from form.form import UserForm, MessageForm

from sqlalchemy import and_
from werkzeug.security import generate_password_hash
from config import MENU_ADMIN_PATIENT, MENU_ADMIN_USER
import os
from init import app
from werkzeug.utils import secure_filename
from datetime import datetime
from model.image import Image
from model.user import User
import uuid

upload = Blueprint('Upload', __name__, url_prefix="/upload")
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@upload.route('/image', methods=["POST"])
# @login_required(role=["0", "1", "2"])
def upload_image():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            now = datetime.now()

            sub_directory = '\\' + str(now.year) + "\\" + str(now.month) + '\\' + str(now.day)
            directory = app.config['UPLOAD_FOLDER'] + sub_directory
            filecode = str(uuid.uuid4())
            extension = os.path.splitext(filename)[1]
            filecode = filecode + extension

            if not os.path.exists(directory):
                os.makedirs(directory)

            file.save(os.path.join(directory, filecode))

            image = Image(name=filename, code=filecode, path=sub_directory, owner=current_user.id)
            db.session.add(image)
            # db.session.flush()
            db.session.commit()
            ret = {
                "code": 200,
                "message": "Successful",
                "data": {
                    "id": image.id,
                    "name": image.name,
                    "code": image.code,
                    "path": image.path
                }
            }
            return ret
    else:
        return "1"


@upload.route('/view/<id>', methods=["GET"])
@login_required(role=["0", "1", "2"])
def download_image(id):
    image = Image.query.get(id)
    return send_from_directory(app.config['UPLOAD_FOLDER']+image.path, image.code, as_attachment=True)
