from faker.providers import phone_number

from init import db
from flask import Blueprint, render_template, redirect, request, flash, url_for, make_response
from flask_login import login_required, login_user, logout_user, current_user
from model.user import User
from werkzeug.security import check_password_hash, generate_password_hash
from form.form import LoginForm

auth = Blueprint('Auth', __name__, url_prefix="/")


@auth.route('/', methods=['POST', 'GET'])
def main():
    # return render_template("application.html")
    return redirect(url_for('Auth.login'))


@auth.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'GET':
        return render_template('auth/register.html')
    if request.method == 'POST':
        result = request.form
        email = result.get('email')
        password = result.get('password')
        ip = result.get('ip')
        name = result.get('name')
        role = result.get('role')
        cmt = result.get('cmt')
        phone = result.get('phone')
        address = result.get('address')
        age = result.get('age')
        gender = result.get('gender')
        hash_pass = generate_password_hash(password, method='sha256')

        if email == None or cmt == None or name == None:
            flash("Điền thiếu thông tin", 'warning')
            return redirect(url_for('Auth.register'))
        else:
            flash("Đăng ký tài khoản thành công", 'success')
            user = User(email=email, password=hash_pass, ip=ip, role="2", name=name, address=address,
                        phone_number=phone, id_card=cmt, age=age, gender=gender)
            db.session.add(user)
            db.session.commit()
        return redirect(url_for('Auth.login'))


@auth.route('/login', methods=['POST', 'GET'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate():

            result = request.form
            email = result.get('email')
            password = result.get('password')
            # if email == 'admin' and password == 'admin':

            # list_user = User.query.all()
            check_user = User.query.filter_by(email=email).first()

            if check_user:
                if check_password_hash(check_user.password, password):
                    login_user(check_user)
                    if check_user.role == "0" or check_user.role == "1":
                        return redirect(url_for('User.list'))
                    else:
                        return redirect(url_for('Patient.patient_index'))
                else:
                    flash("Sai thông tin tài khoản", 'warning')
                    response = make_response(render_template('auth/login.html', current_user=current_user, form=form))
                    response.headers["X-Frame-Options"] = "SAMEORIGIN"
                    response.headers['X-Content-Type-Options'] = 'nosniff'
                    response.headers['X-XSS-Protection'] = '1; mode=block'
                    return response
            else:
                flash("Sai thông tin tài khoản", 'warning')
                response = make_response(render_template('auth/login.html', current_user=current_user, form=form))
                response.headers["X-Frame-Options"] = "SAMEORIGIN"
                response.headers['X-Content-Type-Options'] = 'nosniff'
                response.headers['X-XSS-Protection'] = '1; mode=block'
                return response
        else:
            flash("Sai thông tin tài khoản", 'error')
            response = make_response(render_template('auth/login.html', current_user=current_user, form=form))
            response.headers["X-Frame-Options"] = "SAMEORIGIN"
            response.headers['X-Content-Type-Options'] = 'nosniff'
            response.headers['X-XSS-Protection'] = '1; mode=block'
            return response

    if request.method == 'GET':
        if current_user.is_authenticated:
            if current_user.role == "0" or current_user.role == "1":
                return redirect(url_for('User.list'))
            else:
                return redirect(url_for('Patient.patient_index'))
        else:
            response = make_response(render_template('auth/login.html', current_user=current_user, form=form))
            response.headers["X-Frame-Options"] = "SAMEORIGIN"
            response.headers['X-Content-Type-Options'] = 'nosniff'
            response.headers['X-XSS-Protection'] = '1; mode=block'
            return response


@auth.route('/logout', methods=['POST', 'GET'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('Auth.login'))
