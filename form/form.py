from flask_wtf import FlaskForm
from wtforms import Form, validators, StringField, PasswordField, BooleanField, SubmitField, TextAreaField, TimeField, \
    SelectField
from wtforms.validators import DataRequired, Length
class LoginForm(Form):
    email = StringField('Tên đăng nhập', validators=[DataRequired()])
    password = PasswordField('Mật khẩu', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Đăng nhập')


class UserForm(Form):
    email = StringField('Tên đăng nhập', validators=[DataRequired()])
    name = StringField('Họ tên', validators=[DataRequired()])
    id_card = StringField('Số CMT')
    phone_number = StringField('Số điện thoại')
    ip = StringField('Ip')
    address = StringField('Địa chỉ')
    age = StringField('Tuổi')
    gender_select = SelectField("Chọn giới tính")

    password = PasswordField('Mật khẩu', [
        # validators.DataRequired(),
        validators.EqualTo('confirm', message='Mật khẩu không trùng')
    ])
    confirm = PasswordField('Nhập lại mật khẩu')


class MessageForm(FlaskForm):
    message = TextAreaField(
        'Tin nhắn', validators=[
            DataRequired(),
            Length(min=0, max=140)])
    submit = SubmitField('Submit')


class AppointmentForm(FlaskForm):
    doctor_select = SelectField("Chọn bác sĩ", validators=[DataRequired()])
    specialist_select = SelectField("Chọn chuyên khoa")
    title = StringField("Tiêu đề", validators=[DataRequired()])
    content = TextAreaField("Nội dung", validators=[DataRequired()])
    time_start = TimeField("Bắt đầu lúc", validators=[DataRequired()])
    time_finish = TimeField("Kết thúc lúc", validators=[DataRequired()])
    submit = SubmitField('Submit')


class ClinicForm(FlaskForm):
    name = StringField("Tên phòng khám", validators=[DataRequired()])
    description = TextAreaField("Mô tả")
    address = StringField("Địa chỉ phòng khám", validators == [DataRequired()])
    submit = SubmitField('Submit')


class PrescriptionForm(FlaskForm):
    diagnose = TextAreaField("Chẩn đoán", validators=[DataRequired()])
    drug = TextAreaField("Thuốc điều trị", validators=[DataRequired()])
    body = TextAreaField("Lời dặn", validators=[DataRequired()])
    # appointment_id = SelectField("Lịch khám bệnh", validators=[DataRequired()])
    submit = SubmitField('Submit')


class SpecialistForm(FlaskForm):
    name = StringField("Chuyên khoa", validators=[DataRequired()])
    description = TextAreaField("Mô tả")
    # appointment_id = SelectField("Lịch khám bệnh", validators=[DataRequired()])
    submit = SubmitField('Submit')
