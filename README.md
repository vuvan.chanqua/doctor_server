# Bokeh streaming server

### Install dependencies
Cài đặt thư viện cho project

```
$ pip install -r requirements.txt
```

### Config ip 
Cấu hình địa chỉ ip cho client trong file /bokeh_receiver/receiver.py 

```
77: zmq_socket.connect("tcp://192.168.31.251:9000")
```


### Run
Để chạy server chạy file run.sh
```
# Server. Run
bokeh serve --allow-websocket-origin=127.0.0.1:5000 bokeh_receiver
bokeh serve --allow-websocket-origin=127.0.0.1:5006 bokeh_receiver
    and
./app.py
```
Truy cập `http://localhost:5006/bokeh_receiver` trong trình duyệt để theo dõi

python manage.py db migrate
python manage.py seed
